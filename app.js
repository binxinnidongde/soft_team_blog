App({
  onLaunch: function () {
    // 展示本地存储能力
    var that = this
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    wx.login({
      success: function (res) {
        var code = res.code;//发送给服务器的code  
        wx.getUserInfo({
          success: function (res) {
            var userNick = res.userInfo.nickName;//用户昵称  
            var avataUrl = res.userInfo.avatarUrl;
            if (code) {
              wx.request({
                url: 'https://ke.slickghost.com/beta/getOpenid.php',//服务器的地址，现在微信小程序只支持https请求，所以调试的时候请勾选不校监安全域名  
                data: {
                  code: code,
                },
                header: {
                  'content-type': 'application/json'
                },
                success: function (res) {
                  that.globalData.UserID = res.data;
                  wx.request({
                    url: 'https://ke.slickghost.com/beta/addUser.php',//服务器的地址，现在微信小程序只支持https请求，所以调试的时候请勾选不校监安全域名  
                    data: {
                      nick: userNick,
                      avaurl: avataUrl,
                      openid: that.globalData.UserID
                    }
                  })
                }
              })
            }
            else {
              console.log("获取用户登录态失败！");
            }
          }
        })
      },
      fail: function (error) {
        console.log('login failed ' + error);
      }
    })
  },

  globalData: {
    UserID: null
  }
})
