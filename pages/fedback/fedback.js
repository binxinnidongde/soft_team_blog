var util = require('../../utils/util.js');
var UserID
var age
var sex
// var s1 = '男'
// var s2 = '女'
var fedbackContent
//获取应用实例
var app = getApp()
Page({
  data: {
    noteMaxLen: 150,//最多字数
    info: "",
    noteNowLen: 0,//当前字数
  },

  onLoad: function (options) {
    sex = null;
    age = null;
    fedbackContent = null;
    UserID = app.globalData.UserID;
  },

  bindKeyInput1: function (e) {
    sex = e.detail.value;
    this.setData({
      fedbacksex: e.detail.value
    })
  },

  bindKeyInput2: function (e) {
    age = e.detail.value;
    this.setData({
      fedbackage: e.detail.value
    })
  },

  bindKeyInput3: function (e) {
    var that = this;
    fedbackContent = e.detail.value;
    var value = e.detail.value, len = parseInt(value.length);
    if (len > that.data.noteMaxLen) return;
    that.setData({
      info: value,
      noteNowLen: len
    })
  },

  submit: function (e) {
    if (sex == null || age == null) {
      wx.showModal({
        title: '提示',
        content: '个人信息不能为空！',
      })
    }
    else {
      wx.showToast({
      title: '谢谢反馈',
      icon: 'success',
      duration: 4000
    }),
      wx.request({
        url: 'https://ke.slickghost.com/beta/fedback.php',
        data: {
          fedbackage: age,
          fedbacksex: sex,
          fedbackContent: fedbackContent,
          UserID: UserID
        }
      }),
      wx.switchTab({
        url: '../mine/mine',
      })
    }
  }
})