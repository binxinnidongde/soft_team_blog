//获取应用实例
var app = getApp()
var UserID
Page({
  data: {
    inputShowed: false,
    inputVal: ''
  },

  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },

  //取消按钮
  hideInput: function () {
    this.setData({
      inputVal: '',
      inputShowed: false
    });
  },

  //监听输入内容
  inputTyping: function (e) {
    var that = this;
    that.setData({
      inputVal: e.detail.value
    })
  },

  //确认按钮
  searchOK: function (e) {
    var that = this;
    var inputVal = this.data.inputVal;
    if (inputVal != '') {
      wx.navigateTo({
        url: '../search/search?inputVal=' + inputVal
      })
    }
    else {
      wx.showModal({
        title: '提示',
        content: '不能为空！',
      })
    }
    this.setData({
      inputVal: ''
    });
  },

  //清空按钮点击事件
  clearInput: function () {
    this.setData({
      inputVal: ''
    });
  },

  onLoad: function (options) {
    
  },

  onShow: function (options) {
    var that = this;
    wx.request({
      url: 'https://ke.slickghost.com/beta/displayActivity.php',
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
      success: function (res) {
        //将获取到的json数据，存在名字叫array的这个数组中
        that.setData({
          array: res.data
        })
      }
    })
  }
})