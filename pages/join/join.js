var ActID
var UserID
var app = getApp()
var util = require('../../utils/util.js');
Page({
  data: {
    currentTime: util.formatTime(new Date())
  },

  onLoad: function (options) {
    var that = this;
    ActID = options.ActID;
    UserID = app.globalData.UserID;
    console.log(ActID);
    wx.request({
      url: 'https://ke.slickghost.com/beta/judge.php',
      method: 'GET',
      data: {
        ActID: ActID,
        UserID: UserID
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        //将获取到的json数据，存在名字叫activity的这个数组中
        if (res.data != null) {
          that.setData({
            condition: res.data.Identity
          })
        }
        else {
          that.setData({
            condition: -1
          })
        }
      }
    })
  },

  onShow: function (e) {
    var that = this;
    wx.request({
      url: 'https://ke.slickghost.com/beta/activityDetails.php',
      data: {
        ActID: ActID
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        //将获取到的json数据，存在名字叫activity的这个数组中
        that.setData({
          array: res.data
        }),
          wx.request({
            url: 'https://ke.slickghost.com/beta/allDynamic.php',
            data: {
              ActID: ActID
            },
            headers: {
              'Content-Type': 'application/json'
            },
            success: function (res) {
              //将获取到的json数据，存在名字叫activity的这个数组中
              that.setData({
                dynamic: res.data
              })
            }
          })
      }
    })
  },
  
  joinin: function (res) {
    wx.request({
      url: 'https://ke.slickghost.com/beta/join.php',
      data: {
        UserID: UserID,
        ActID: ActID
      },
      headers: {
        'Content-Type': 'application/json'
      }
    }),
      wx.showToast({
        title: '加入成功',
        icon: 'success',
        duration: 3000
      }),
      wx.switchTab({
        url: '../index/index',
      })
  }
})

