var app = getApp
Page({
  /*页面的初始数据*/
  data: {
    //用户个人信息
    userInfo: {
      avatarUrl: "",//用户头像
      nickName: "",//用户昵称
    }
  },

  /*生命周期函数--监听页面加载*/
  onLoad: function () {
    var that = this;
    /*获取用户信息*/
    wx.getUserInfo({
      success: function (res) {
        // console.log(res);
        var avatarUrl = 'userInfo.avatarUrl';
        var nickName = 'userInfo.nickName';
        that.setData({
          [avatarUrl]: res.userInfo.avatarUrl,
          [nickName]: res.userInfo.nickName,
        })
      }
    })
  },

  myActivity: function () {
    wx.navigateTo({
      url: '../myActivity/myActivity',
    })
  },

  myDynamic: function () {
    wx.navigateTo({
      url: '../record/record',
    })
  },

  myreward: function () {
    wx.navigateTo({
      url: '../reward/reward',
    })
  },
  
  myuser: function () {
    wx.navigateTo({
      url: '../fedback/fedback',
    })
  }
})