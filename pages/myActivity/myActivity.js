var UserID
//获取应用实例
var app = getApp()
Page({
  data: {
    defaultSize: 'default',
    disabled: false,
    plain: false,
    loading: false,
    disabled: false
  },

  onLoad: function (options) {
    var that = this;
    UserID = app.globalData.UserID;
    wx.request({
      url: 'https://ke.slickghost.com/beta/chooseActivity.php',
      data: {
        UserID: UserID
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        //将获取到的json数据，存在名字叫array的这个数组中
        if (res.data != null) {
          that.setData({
            array: res.data
          })
        }
        else {
          that.setData({
            array: 0
          })
        }
      }
    })
  },

  //事件处理函数
  bindViewTap: function (e) {
    var id = e.target.id;
    wx.navigateTo({
      url: '../details/details?ActID=' + id
    })
  }
})
